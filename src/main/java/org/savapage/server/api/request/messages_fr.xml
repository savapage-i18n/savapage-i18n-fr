<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the SavaPage project <https://savapage.org>
Copyright (c) 2020 Datraverse B.V. | Author: Rijk Ravestein 

SPDX-FileCopyrightText: © 2020 Datraverse BV <info@datraverse.com>
SPDX-License-Identifier: AGPL-3.0-or-later

SavaPage is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.
-->
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>

    <entry key="msg-single-parm">{0}</entry>

    <entry key="msg-value-cannot-be-empty">{0} ne peut être vide.</entry>
    <entry key="msg-value-invalid">{0} "{1}" est invalide.</entry>

    <entry key="msg-file-not-present">Le fichier [{0}] n'existe pas.</entry>
    <entry key="msg-no-email-address-available">Pas d'adresse e-mail valide.</entry>

    <entry key="msg-saved-ok">Sauvegarde effectuée.</entry>
    <entry key="msg-apply-ok">Appliqué correctement.</entry>
    <entry key="msg-deleted-ok">Supprimé correctement.</entry>
    <entry key="msg-apply-no-changes">Pas de changement à appliquer.</entry>
    <entry key="msg-refresh-ok">Rafraîchi correctement.</entry>
    <entry key="msg-action-failed">Échec de l'action.</entry>

    <!-- ReqAccountGet -->

    <entry key="msg-account-not-found">Le compte "{0}" n'a pas été trouvé.</entry>

    <!-- ReqConfigPropGet -->

    <entry key="msg-config-prop-not-found">L'option "{0}" n'a pas été trouvée.</entry>

    <!-- ReqConfigPropsSet -->

    <entry key="msg-config-props-applied">Les options de configuration ont été appliquées.</entry>
    <entry key="msg-config-props-error">La valeur "{0}" n'est pas valide.</entry>
    <entry key="msg-config-props-applied-rescheduled">Les options de configuration ont été appliquées et les tâches ont été re-programmées.</entry>
    <entry key="msg-config-props-applied-mail-print-stopped">Les options de configuration ont été appliquées et l'impression mail a été stoppée.</entry>

    <entry key="msg-config-props-applied-papercut-print-monitor-stopped">Les options de configuration ont été appliquées et la surveillance des impressions PaperCut a été stoppée.</entry>
    <entry key="msg-config-props-applied-papercut-print-monitor-started">Les options de configuration ont été appliquées et la surveillance des impressions PaperCut a été démarrée.</entry>

    <entry key="msg-printer-not-found">L'imprimante proxy "{0}" n'a pas été trouvée.</entry>
    <entry key="msg-cups-notifier-method-changed">Les notifications CUPS utilisent maintenant la méthode {0}.</entry>

    <!-- ReqDeviceGet -->

    <entry key="msg-device-not-found">L'appareil "{0}" n'a pas été trouvé.</entry>

    <!-- ReqDeviceDelete -->

    <entry key="msg-device-delete-reader-in-use">Le lecteur de cartes réseau "{0}" ne peut pas être supprimé car il est utilisé par le terminal "{1}".</entry>
    <entry key="msg-device-deleted-ok">L'appareil a été correctement supprimé.</entry>

    <!-- ReqDeviceSet -->

    <entry key="msg-device-duplicate-name">L'appareil "{0}" existe déjà.</entry>
    <entry key="msg-device-port-error">Veuillez entrer le port de l'IP.</entry>
    <entry key="msg-device-card-reader-missing">Veuillez entrer un lecteur de cartes réseau.</entry>
    <entry key="msg-device-card-reader-not-found">Le lecteur de cartes réseau "{0}" n'a pas été trouvé.</entry>
    <entry key="msg-device-card-reader-reserved-for-terminal">Le lecteur de cartes réseau "{0}" es déjà réservé par le terminal "{1}".</entry>
    <entry key="msg-device-card-reader-reserved-for-printer">Le lecteur de cartes réseau "{0}" es déjà réservé par l'imprimante "{1}".</entry>
    <entry key="msg-device-card-reader-reserved-for-printer-group">Le lecteur de cartes réseau "{0}" es déjà réservé par le groupe d'imprimantes "{1}".</entry>
    <entry key="msg-device-idle-seconds-error">Veuillez entrer le délai avant déconnexion en seconde (entrez "0" pour désactiver la connexion automatique).</entry>
    <entry key="msg-device-single-proxy-printer-needed">Le mode d'impression rapide requiert une unique imprimante proxy.</entry>
    <entry key="msg-device-proxy-printer-or-group-needed">L'authentification d'impression proxy requiert une unique imprimante ou groupe d'imprimantes proxy.</entry>
    <entry key="msg-device-proxy-printer-either-group">Veuillez sélectionner une seule imprimante ou groupe d'imprimantes, pas les deux.</entry>
    <entry key="msg-device-printer-not-found">L'imprimante proxy "{0}" n'a pas été trouvée.</entry>
    <entry key="msg-device-printer-group-not-found">Le groupe d'imprimantes proxy "{0}" n'a pas été trouvé.</entry>
    <entry key="msg-device-created-ok">L'appareil a été correctement créé.</entry>
    <entry key="msg-device-saved-ok">L'appareil a été correctement enregistré.</entry>

    <!-- ReqDbBackup -->

    <entry key="msg-db-backup-busy">Création de la sauvegarde...</entry>

    <!-- ReqDocLogRefund -->
    <entry key="msg-refund-ok">Remboursement effectué avec succès.</entry>

    <!-- ReqDocLogTicketReopen -->
    <entry key="msg-ticket-reopen-ok">La tâche a été correctement ré-ouvert.</entry>

    <!-- ReqI18nCacheClear -->
    <entry key="msg-i18n-cache-cleared-ok">Le cache i18n a été correctement nettoyé.</entry>

    <!-- ReqInboxClear -->
    <entry key="msg-inbox-clear-none">Pas de document à supprimer.</entry>
    <entry key="msg-inbox-clear-single">Le document reçu a été supprimé.</entry>
    <entry key="msg-inbox-clear-multiple">Les documents reçus ont été supprimés.</entry>

    <!-- ReqInboxRestorePrintIn -->
    <entry key="msg-inbox-restore-printin-added">Le document a été ajouté à la boîte de réception.</entry>
    <entry key="msg-inbox-restore-printin-replaced">Le document a été replacé dans la boîte de réception.</entry>
    <entry key="msg-inbox-restore-printin-unavailable">Le document n'est pas accessible.</entry>

    <!-- ReqDocLogStoreDelete -->
    <entry key="msg-doclog-store-unavailable">Le document n'est pas accessible.</entry>

    <!-- ReqJobTicketExec -->

    <entry key="msg-jobticket-option-mismatch">{0} ne peut être imprimé sur {1} "{2}".</entry>
    <entry key="msg-jobticket-print-none">Pas de tâche d'impression trouvée.</entry>
    <entry key="msg-jobticket-print-ok">La tâche {0} est en cours d'impression . . .</entry>
    <entry key="msg-jobticket-settled-ok">La tâche a été effectuée {0} correctement.</entry>

    <!-- ReqLogin -->

    <entry key="msg-auth-mode-not-available">L'authentification "{0}" n'est pas disponible.</entry>
    <entry key="msg-login-not-possible">La connexion est impossible momentanément. Veuillez réessayer plus tard.</entry>
    <entry key="msg-user-home-dir-create-error">Le dossier SafePages n'a pas pu être créé.</entry>
    <entry key="msg-login-install-mode">L'application est en cours d'installation. Veuillez patienter quelques minutes avant de réessayer.</entry>
    <entry key="msg-login-app-config">L'application est en cours de configuration. Veuillez patienter quelques minutes avant de réessayer.</entry>
    <entry key="msg-login-totp-required">Le TOTP est requis.</entry>
    <entry key="msg-login-failed">L'authentification a échoué.</entry>
    <entry key="msg-login-missing-privilege">Privilèges manquants.</entry>
    <entry key="msg-login-as-internal-admin">Un paramétrage de l'application est nécessaire. Veuillez contacter votre administrateur système.</entry>
    <entry key="msg-login-unregistered-card">La carte n'est pas enregistrée.</entry>
    <entry key="msg-login-card-unknown">{0} WebApp {1} Login: la carte "{2}" n'est pas enregistrée ({3}).</entry>

    <entry key="msg-login-no-pin-available">{0} WebApp Login: pas de PIN disponible pour le code {1} ({2}).</entry>
    <entry key="msg-login-disabled">{0} WebApp {1} Login: le compte utilisateur "{2}" a été désactivé ({3}).</entry>
    <entry key="msg-login-no-person">{0} WebApp {1} Login: le compte utilisateur "{2}" n'est pas un individu ({3}).</entry>
    <entry key="msg-login-no-admin-rights">{0} WebApp {1} Login: le compte utilisateur "{2}" n'est pas un administrateur ({3}).</entry>
    <entry key="msg-login-no-access-to-role">{0} WebApp {1} Login: le compte utilisateur "{2}" n'est pas un {3} ({4}).</entry>
    <entry key="msg-login-non-privileged">{0} WebApp {1} Login: le compte utilisateur "{2}" n'a pas le privilège "{3}".</entry>
    <entry key="msg-login-user-not-present">{0} WebApp {1} Login: le compte utilisateur "{2}" n'existe pas ({3}).</entry>
    <entry key="msg-login-denied">{0} WebApp {1} Login: accès refusé pour l'utilisateur "{2}" ({3}).</entry>
    <entry key="msg-login-invalid-password">{0} WebApp Login: le mot de passe de "{1}" est invalide ({2}).</entry>
    <entry key="msg-login-invalid-number">{0} WebApp Login: l'ID "{1}" est invalide ({2}).</entry>
    <entry key="msg-login-invalid-pin">{0} WebApp Login: PIN invalide pour l'ID "{1}" ({2}).</entry>

    <entry key="msg-login-conflict">Vous êtes déjà connecté en tant que {0}.</entry>

    <entry key="msg-login-yubikey-connection-error">{0} WebApp Login: YubiKey "{1}". Pas de connexion : {2}</entry>

    <entry key="msg-login-user-lazy-create-success">{0} WebApp: l'utilisateur "{1}" a été créé suite à sa première connexion.</entry>
    <entry key="msg-login-user-lazy-create-failed">{0} WebApp: échec de la création de l'utilisateur "{1}" suite à sa première connexion.</entry>

    <entry key="msg-setup-is-needed">SavaPage doit être configuré et ne peut pas être utilisé auparavant. Veuillez accéder à la section Options et vérifier les paramètres : {0}</entry>
    <entry key="msg-change-internal-admin-password">Veuillez changer le mot de passe par défaut de l'administrateur interne.</entry>
    <entry key="msg-card-registered-ok">La carte a été enregistrée correctement.</entry>

    <!-- ======================================== -->
    <!-- Membership messages -->
    <!-- ======================================== -->
    <entry key="msg-membership-exceeded-user-limit">Vous dépassez votre limite de {0} participants. Vous êtes invité à réduire le nombre d'utilisateurs ou contacter {1} pour une nouvelle {2}.</entry>
    <entry key="msg-membership-expired">Votre {0} a expiré. Vous êtes invité à contacter {1} pour une nouvelle {2}.</entry>
    <entry key="msg-membership-visit">Il reste {0} jours restants à votre période de {1}.</entry>
    <entry key="msg-membership-visit-expired">Votre période de {0} a expirée. Vous êtes invité à contacter {1} pour une {2}.</entry>
    <entry key="msg-membership-wrong-product">Votre {0} n'est pas valide pour ce produit. Vous êtes invité à contacter {1} pour la {2} correcte.</entry>
    <entry key="msg-membership-version">Votre {0} n'est pas valide pour cette version du produit. Vous êtes invité à contacter {1} pour mettre à jour votre {2}.</entry>

    <!-- ReqJobTicketSave -->
    <entry key="msg-outbox-changed-jobticket">La tâche a été modifiée.</entry>
    <entry key="msg-outbox-changed-jobticket-none">Aucune tâche à modifier.</entry>
    <entry key="msg-outbox-jobticket-blocked-options">Le document a été préparé pour "{0}" ou "{1}" : ces options ne peuvent être modifiées.</entry>

    <!-- ReqJobTicketPrintClose -->
    <entry key="msg-outbox-jobticket-print-close">La tâche a été fermée.</entry>
    <entry key="msg-outbox-jobticket-print-close-none">Aucune tâche à fermer.</entry>

    <!-- ReqJobTicketPrintCancel -->
    <entry key="msg-outbox-jobticket-print-cancel-success">La tâche d'impression a été annulées.</entry>
    <entry key="msg-outbox-jobticket-print-cancel-failed">La tâche d'impression a échouée.</entry>
    <entry key="msg-outbox-jobticket-print-cancel-none">Aucune tâche d'impression à annuler.</entry>

    <!-- ReqMailTest -->
    <entry key="msg-email-invalid">L'email "{0}" est invalide.</entry>
    <entry key="mail-test-subject">SavaPage Test mail</entry>
    <!-- This text should be on one (1) line: line feeds are picked up by 
        the mailer -->
    <entry key="mail-test-body">Bonjour,&#60;p&#62;Ceci est un message de test de
        {0}.&#60;/p&#62;--&#60;br&#62;{1}</entry>
    <entry key="msg-mail-sent">Mail envoyé à {0}</entry>

    <!-- ReqOutboxCancelJob, ReqJobTicketCancel -->
    <entry key="msg-outbox-cancelled-job">La tâche d'impression a été annulée.</entry>
    <entry key="msg-outbox-cancelled-job-none">Aucune tâche d'impression à annuler n'a été trouvée.</entry>
    <entry key="msg-outbox-cancelled-jobticket">La tâche d'impression a été annulée.</entry>
    <entry key="msg-outbox-cancelled-jobticket-none">Aucune tâche d'impression à annuler n'a été trouvée.</entry>

    <!-- ReqOutboxReleaseJob -->
    <entry key="msg-outbox-release-job">Tâches d'impression à effectuer.</entry>
    <entry key="msg-outbox-release-job-none">Aucune tâche d'impression à effectuer n'a été trouvée.</entry>

    <!-- ReqOutboxClear -->
    <entry key="msg-outbox-cancelled-none">Aucune tâche d'impression trouvée.</entry>
    <entry key="msg-outbox-cancelled-one">Tâche d'impression {0} à annuler.</entry>
    <entry key="msg-outbox-cancelled-multiple">{0} tâches d'impression annulés.</entry>

    <!-- ReqOutboxExtend -->
    <entry key="msg-outbox-extended-none">Pas de tâches d'impressions à étendre.</entry>
    <entry key="msg-outbox-extended-one">La tâche d'impression {0} a été étendue à {1} minutes.</entry>
    <entry key="msg-outbox-extended-multiple">{0} tâches d'impressions ont été étendues à {1} minutes.</entry>

    <!-- ReqQueueEnable -->

    <entry key="msg-queue-urlpath-not-found">File d'attente "{0}" introuvable.</entry>

    <entry key="msg-queue-enabled">La file d'attente "{0}" a été activée.</entry>
    <entry key="msg-queue-disabled">La file d'attente "{0}" a été désactivée.</entry>

    <entry key="msg-queue-enabled-pub">La file d'attente "{0}" a été activée par l'utilisateur "{1}".</entry>
    <entry key="msg-queue-disabled-pub">La file d'attente "{0}" a été désactivée par l'utilisateur "{1}".</entry>

    <entry key="msg-queue-enabled-already">La file d'attente "{0}" est déjà activée.</entry>
    <entry key="msg-queue-disabled-already">La file d'attente "{0}" est déjà désactivée.</entry>

    <!-- ReqQueueGet -->

    <entry key="msg-queue-not-found">La file d'attente "{0}" est introuvable.</entry>

    <!-- ReqQueueSet -->

    <entry key="msg-queue-saved-ok">La file d'attente a été correctement sauvegardée.</entry>
    <entry key="msg-queue-created-ok">La file d'attente a été correctement créée.</entry>
    <entry key="msg-queue-duplicate-path">La file d'attente "{0}" existe déjà.</entry>
    <entry key="msg-queue-reserved-path">La file d'attente "{0}" est réservée.</entry>
    <entry key="msg-queue-empty-path">L'adresse URL ne peut être vide.</entry>

    <!-- ReqPdfPropsSetValidate -->
    <entry key="msg-pdf-identical-owner-user-pw">L'utilisateur et le mot de passe PDF doivent être différents.</entry>

    <!-- ReqPrinterSet -->
    <entry key="msg-printer-saved-ok">L'imprimante proxy a été correctement sauvegardée.</entry>
    <entry key="msg-printer-not-found">L'imprimante proxy "{0}" n'a pas été trouvée.</entry>
    <entry key="msg-jobticket-group-not-found">Le groupe d'imprimantes proxy "{0}" n'a pas été trouvé.</entry>
    <entry key="msg-jobticket-group-not-entered">Veuillez entrer un groupe d'imprimantes pour la tâche.</entry>
    <entry key="msg-jobticket-papercut-not-allowed">La tâche d'impression n'est pas autorisée, car l'imprimante est gérée par PaperCut.</entry>

    <!-- ReqPrinterSync -->
    <entry key="msg-printer-sync-ok">La synchronisation de CUPS s'est déroulée avec succès.</entry>
    <entry key="msg-printer-connection-broken">La connexion avec les imprimantes proxy est cassée.</entry>

    <!-- ReqPrinterSnmp -->
    <entry key="msg-printer-snmp-ok">Les données de l'appareil ont été récupérées et téléchargées . . .</entry>
    <entry key="msg-printer-snmp-busy">Les données de l'appareil sont en train d'être récupérées. Veuillez réessayer plus tard.</entry>

    <!-- ReqPrinterPrint -->
    <entry key="msg-print-exceeds-pagelimit">Le nombre de pages à imprimer de "{0}" excède le nombre maximum autorisé de "{1}".</entry>
    <entry key="msg-print-exceeds-jobticket-pagelimit">Le nombre de pages à imprimer de "{0}" excède "{1}" : veuillez créer une tâche.</entry>
    <entry key="msg-ecoprint-pending">L'impression Eco est en cours de préparation. Veuillez patienter quelques minutes et réessayer.</entry>
    <entry key="msg-select-single-pdf-filter">La sélection de multiples filtres PDF n'est pas supportée : veuillez sélectionner un unique filtre.</entry>
    <entry key="msg-print-auth-pending">Une autre tâche d'impression est en attente d'authentification. Veuillez réessayer plus tard.</entry>
    <entry key="msg-print-users-missing-in-papercut">Ces utilisateurs sont manquants dans PaperCut : {0}</entry>
    <entry key="msg-print-service-unavailable">Le service d'impression est momentanément indisponible ou surchargé. Veuillez réessayer plus tard.</entry>
    <entry key="msg-copyjob-title-missing">Entrez le titre du document original.</entry>
    <entry key="msg-copyjob-pages-invalid">Entez le nombre de pages du document original.</entry>
    <entry key="msg-copyjob-media-source-missing">Sélectionnez la source média pour la tâche de copie.</entry>
    <entry key="msg-print-select-option-required">La sélection de "{0}" est requise.</entry>

    <entry key="msg-print-booklet-collate-mismatch">Les livrets doivent utiliser l'assemblage de copies.</entry>

    <entry key="msg-print-orientation-mismatch-single">Vous avez sélectionné {0} et l'orientation "{1}" lors de la prévisualisation, alors que le document a imprimer à l'orientation "{2}". Ajustez vos paramètres d'impression et réessayez.</entry>

    <entry key="msg-print-orientation-mismatch-multiple">Vous avez sélectionner {0} et l'orientation "{1}" lors de la prévisualisation, alors que {2} des {3} documents à imprimer ont l'orientation "{4}". Désélectionnez les options de finitions, ou imprimez les documents séparément.</entry>

    <!-- ReqUrlPrint -->
    <entry key="msg-url-print-unknown-content-type">Erreur lors du téléchargement de [{0}] : type de contenu inconnu.</entry>
    <entry key="msg-url-print-error">Erreur lors du téléchargement de [{0}] : {1}</entry>

    <!--  ReqUserHomeClean -->
    <entry key="msg-userhome-clean-started">Le nettoyage de l'utilisateur {0} a débuté.</entry>
    <entry key="msg-userhome-clean-busy">Le nettoyage de l'utilisateur {0} est déjà en cours.</entry>

    <!-- ReqUserGet -->
    <entry key="msg-user-not-found">L'utilisateur "{0}" n'a pas été trouvé.</entry>

    <!-- ReqUserSet -->
    <entry key="msg-user-saved-ok">L'utilisateur a été correctement enregistré.</entry>
    <entry key="msg-user-created-ok">L'utilisateur a été correctement créé.</entry>

    <!-- ReqUserCardQuickSearch -->
    <entry key="msg-usercard-unknown">Carte inconnue.</entry>

    <!-- ReqUserGroupGet -->
    <entry key="msg-usergroup-not-found">Le groupe d'utilisateurs "{0}" n'a pas été trouvé.</entry>
    <entry key="msg-usergroup-updated">Le groupe d'utilisateurs "{0}" a été correctement mis à jour.</entry>

    <!-- ReqUserGroupsAddRemove -->
    <entry key="msg-no-groups">Sélectionnez les groupes à ajouter ou supprimer.</entry>
    <entry key="msg-groups-added">Groupes ajoutés : {0}.</entry>
    <entry key="msg-groups-removed">Groupes supprimés : {0}.</entry>

    <!-- ReqUserPasswordErase -->
    <entry key="msg-user-password-erased-ok">Le mot de passe a été correctement supprimé.</entry>

    <!-- ReqUserDelegateGroupsPreferred -->
    <entry key="msg-delegate-groups-preferred-max">Le nombre maximum de groupes délégués a été atteint : la préférence n'a pas été enregistrée.</entry>

</properties>
